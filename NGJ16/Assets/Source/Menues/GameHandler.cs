﻿using UnityEngine;
using GamepadInput;
using UnityEngine.SceneManagement;

public class GameHandler : MonoBehaviour
{

    private static GameHandler _instance;
    private Scene _currentScene;

    public static GameHandler Instance
    {
        get { return _instance; }
    }

    public int TeamOneScore { get; set; }
    public int TeamTwoScore { get; set; }
    public int MaxScore { get; set; }

    public void SetScores(int teamOne, int teamTwo, int maxScores)
    {
        TeamOneScore = teamOne;
        TeamTwoScore = teamTwo;
        MaxScore = maxScores;
    }

    public void Awake()
    {
        // This checks if the game handler already exists
        // and if it does, deletes itself, so we don't have duplicates.
        if (_instance != null)
        {
            if (_instance != this)
            {
                Destroy(gameObject);
            }
        }
        else
        {
            _instance = this;
            _currentScene = Scene.Main;
            DontDestroyOnLoad(gameObject);

            
        }
    }

	// Use this for initialization
	void Start () {
	    if (_instance == this)
	    {
            InAudio.Music.PlayWithFadeIn(AudioEffects.Instance.MainMenu, 2.0f);     
        }
	}

    public void GoToScoreScreen()
    {
        _currentScene = Scene.Score;
        InAudio.Music.Stop(AudioEffects.Instance.Battle);
        SceneManager.LoadScene("ScoreScreen");

    }
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetKeyDown(KeyCode.Escape))
	    {
	        Application.Quit();
	    }
	    switch (_currentScene)
	    {
	        case Scene.Main:
	            if (GamePad.GetButtonDown(GamePad.Button.Start, GamePad.Index.Any) || Input.GetKeyDown(KeyCode.Return))
	            {
	                _currentScene = Scene.Arena;
                    InAudio.Music.Stop(AudioEffects.Instance.MainMenu);
                    InAudio.Music.Play(AudioEffects.Instance.Battle);
                    SceneManager.LoadScene("Arena");
	            }
	            break;
            case Scene.Arena:

	            break;
            case Scene.Score:
                if (GamePad.GetButtonDown(GamePad.Button.Start, GamePad.Index.Any) || Input.GetKeyDown(KeyCode.Return))
                {
                    InAudio.StopAllAndMusic();
                    InAudio.Music.Play(AudioEffects.Instance.MainMenu);

                    _currentScene = Scene.Main;
                    System.GC.Collect();
                    SceneManager.LoadScene("MainMenu");
                }
                break;
	    }
	}

    public enum Scene
    {
        Main, Arena, Score
    }
}
