﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TintingAnim : MonoBehaviour
{

    public float TimeBetweenSwitching = 1;
    public Image Background;

	// Use this for initialization
	void Start ()
	{
        //Debug.Log("MESSAGE HERE!");
        FadeToColour();
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    private void FadeToColour()
    {
        Color color;
        int index = (int)(Random.value*5);
        switch (index)
        {
            case 0:
                color = Color.blue;
                break;
            case 1:
                color = Color.red;
                break;
            case 2:
                color = Color.yellow;
                break;
            case 3:
                color = Color.green;
                break;
            case 4:
                color = Color.white;
                break;
            default:
                color = Color.magenta;
                break;
        }
        Background.CrossFadeColor(color, 1, true, false);
        Invoke("FadeToColour", TimeBetweenSwitching);
    }
}
