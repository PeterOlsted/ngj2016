﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScoreMenu : MonoBehaviour {

    public Text T1ScoreText, T2ScoreText;
    public Image T1ScoreImage, T2ScoreImage;
    public List<Sprite> T1ScoreSprites, T2ScoreSprites;
    public float ScoreFillSpeed;
    public float FontSizeMultiplier;
    public float ScreenShakeMultiplier;
    public BlinkingText PressToContinue;

    public RectTransform T1Panel, T2Panel;

    private Vector3 _t1PanelPos, _t2PanelPos;
    private bool _started;
    private bool _t1Done, _t2Done;
    private float _timePassed;

    // Use this for initialization
    void Start ()
    {
        _t1PanelPos = T1Panel.localPosition;
        _t2PanelPos = T2Panel.localPosition;
        if (GameHandler.Instance != null)
        {
            SetScore(GameHandler.Instance.TeamOneScore, GameHandler.Instance.TeamTwoScore, GameHandler.Instance.MaxScore);
        }
    }
	
	// Update is called once per frame
	void Update ()
	{
	    if (_started)
	    {
	        _timePassed += Time.deltaTime;
	        if (!_t1Done)
	        {
	            float offsetX = Random.value > .5f ? -1 : 1;
	            offsetX = offsetX * (_timePassed * ScreenShakeMultiplier * (Random.value + .5f));
	            float offsetY = Random.value > .5f ? -1 : 1;
	            offsetY = offsetY * (_timePassed * ScreenShakeMultiplier * (Random.value + .5f));
                T1Panel.localPosition = new Vector3(_t1PanelPos.x + offsetX, _t1PanelPos.y + offsetY, _t1PanelPos.z);
	        }
	        if (!_t2Done)
	        {
	            float offsetX = Random.value > .5f ? -1 : 1;
	            offsetX = offsetX * (_timePassed * ScreenShakeMultiplier * (Random.value + .5f));
	            float offsetY = Random.value > .5f ? -1 : 1;
	            offsetY = offsetY * (_timePassed * ScreenShakeMultiplier * (Random.value + .5f));
	            T2Panel.localPosition = new Vector3(_t2PanelPos.x + offsetX, _t2PanelPos.y + offsetY, _t2PanelPos.z);
	        }
	    }
	}

    [ContextMenu("Test set score")]
    public void TestSetScore()
    {
        SetScore(142.2f, 165.2f, 165.2f);
    }

    public void SetScore(float teamOneScore, float teamTwoScore, float maxScore)
    {
        _timePassed = 0;
        _started = true;
        InAudio.Play(gameObject, AudioEffects.Instance.WinSwipe);
        StartCoroutine(AnimateToScore(T1ScoreText, T1ScoreImage, teamOneScore, maxScore, T1ScoreSprites, true));
        StartCoroutine(AnimateToScore(T2ScoreText, T2ScoreImage, teamTwoScore, maxScore, T2ScoreSprites, false));

    }


    
    public IEnumerator AnimateToScore(Text textObj, Image image, float score, float maxScore, List<Sprite> spriteList, bool teamOne)
    {
        float curScore = 0f;
        while (curScore < score)
        {
            int index = (int)((curScore / maxScore) * spriteList.Count);
            textObj.text = ((int) curScore).ToString();
            textObj.fontSize = 100 + (int)(curScore*FontSizeMultiplier);
            image.sprite = spriteList[Mathf.Min(spriteList.Count- 1, index)];
            curScore += ScoreFillSpeed;
            yield return new WaitForEndOfFrame();
        }
        if (teamOne)
        {
            _t1Done = true;
        }
        else
        {
            _t2Done = true;
        }
        if (_t1Done && _t2Done)
        {
            InAudio.Stop(gameObject, AudioEffects.Instance.WinSwipe);
            string name = SceneManager.GetActiveScene().name;
            InAudio.Play(gameObject, AudioEffects.Instance.WinBling).OnCompleted += (o, node) =>
            {
                PressToContinue.enabled = true;
                if (name == SceneManager.GetActiveScene().name)
                {
                    InAudio.Music.PlayWithFadeIn(AudioEffects.Instance.ScoreScreen, .5f);
                }

            };

        }
    } 
}
