﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BlinkingText : MonoBehaviour
{
    public float FlashSpeed;
    private Text _text;
    private bool _forward;

    // Use this for initialization
	void Start ()
	{
	    _text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
	    if (_forward)
	    {
	        if (_text.color.a < 1f)
	        {
	            _text.color = new Color(_text.color.r, _text.color.g, _text.color.b, _text.color.a + FlashSpeed);
	        }
	        else
	        {
	            _forward = !_forward;
	        }
	    }
	    else
	    {
            if (_text.color.a > 0)
            {
                _text.color = new Color(_text.color.r, _text.color.g, _text.color.b, _text.color.a - FlashSpeed);
            }
            else
            {
                _forward = !_forward;
            }
        }
	}
}
