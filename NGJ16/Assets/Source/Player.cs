﻿using System.Collections;
using UnityEngine;
using GamepadInput;

public class Player : MonoBehaviour {
    public GamePad.Index Index;
    public Team Team;

    public float Impulse;

    void OnTriggerEnter(Collider other)
    {
        var otherPlayer = other.GetComponent<Player>();

        if (otherPlayer != null && otherPlayer.Team != Team)
        {
            var myHuman = this as Human;
            var otherHuman = otherPlayer as Human;
            var animator = GetComponentInChildren<Animator>();

            if (otherPlayer is Squid)
            {
                //animator.SetTrigger("Dead");
                CameraShake.Instance.Shake();
                Destroy(other.gameObject);
                InAudio.PlayPersistent(transform.position, AudioEffects.Instance.Splash);
                LevelHandling.Instance.NextRound();
            }
            if (myHuman != null && otherHuman != null)
            {
                animator.SetTrigger("Push");

                if (Team == Team.TEAM_ONE)
                {
                    Vector3 toOther = otherHuman.transform.position - transform.position;
                    Vector3 toMe = -toOther;
                    toOther.y = 0;
                    toMe.y = 0;

                    StartCoroutine(PushAway(toOther, toMe, otherHuman, myHuman)); 
                }

            }
        }
    }

    IEnumerator PushAway(Vector3 toOther, Vector3 toMe, Human otherHuman, Human myHuman)
    {
        yield return new WaitForSeconds(0.1f);
        InAudio.PlayPersistent(transform.position, AudioEffects.Instance.Bounce);
        CameraShake.Instance.Shake();
        otherHuman.ApplyImpulse(toOther, Impulse);
        myHuman.ApplyImpulse(toMe, Impulse);
    }

    void OnTriggerExit(Collider other)
    {
        if(other.tag == "Arena")
        {
            CameraShake.Instance.Shake();
            var human = this as Human;
            var squid = this as Squid;
            Vector3 dir = Vector3.zero - transform.position;
            dir.y = 0;

            if (human != null)
            {
                human.ApplyImpulse(dir, Impulse / 6);
            }

            if (squid != null)
            {
                squid.ApplyImpulse(dir, Impulse / 6);
            }
            InAudio.PlayPersistent(transform.position, AudioEffects.Instance.Bounce);
        }
       
    }
}
