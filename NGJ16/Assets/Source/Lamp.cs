﻿using UnityEngine;
using System.Collections;

public class Lamp : MonoBehaviour
{
    public float SpeedMultiplier = 1;
    public float RotationMultiplier = 1;

    private float _origX, _origY;
    private float _offsetX, _offsetY;
    private float _curTime;
    private int _dir;

	// Use this for initialization
	void Start ()
	{
	    _origX = transform.eulerAngles.x;
	    _origY = transform.eulerAngles.y;
	    _dir = Random.value < .5 ? -1 : 1;
	}
	
	// Update is called once per frame
	void Update ()
	{
	    _curTime += Time.deltaTime * Random.value * SpeedMultiplier * _dir;
	    Rotate();
	}

    private void Rotate()
    {
        _offsetX = Mathf.Sin(_curTime) * RotationMultiplier;
        _offsetY = Mathf.Cos(_curTime) * RotationMultiplier;
        transform.eulerAngles = new Vector3(_origX + _offsetX, _origY + _offsetY, transform.eulerAngles.z);
    }
}
