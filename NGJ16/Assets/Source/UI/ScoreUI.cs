﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class ScoreUI : MonoBehaviour
{

    public Text T1ScoreText, T2ScoreText;
    public Image T1ScoreImage, T2ScoreImage;
    public List<Sprite> T1ScoreSprites, T2ScoreSprites;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SetScore(float teamOneScore, float teamTwoScore, float maxScore)
    {
        T1ScoreText.text = ((int)teamOneScore).ToString();
        T2ScoreText.text = ((int)teamTwoScore).ToString();
        int t1Index = (int)((teamOneScore / maxScore) * T1ScoreSprites.Count);
        int t2Index = (int)((teamTwoScore / maxScore) * T2ScoreSprites.Count);
        t1Index = t1Index >= T1ScoreSprites.Count ? T1ScoreSprites.Count - 1 : t1Index;
        t2Index = t2Index >= T2ScoreSprites.Count ? T2ScoreSprites.Count - 1 : t2Index;
        T1ScoreImage.sprite = T1ScoreSprites[t1Index];
        T2ScoreImage.sprite = T2ScoreSprites[t2Index];
    }
}
