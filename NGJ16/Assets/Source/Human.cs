﻿using UnityEngine;
using GamepadInput;

public class Human : Player {
    

    public float acceleration = 40;
    public float friction = 4;

    Vector3 velocity;
    Vector3 input;

    public bool UseGamepad = false;

    void GetInput()
    {
        GamepadState state = GamePad.GetState(Index); 
        input = Vector3.zero;

        input.x = state.LeftStickAxis.x;
        input.z = state.LeftStickAxis.y;

        input.Normalize();
    }

    void GetKeyboardInput()
    {
        //get the input
        input = Vector3.zero;
        if (Input.GetKey(KeyCode.RightArrow)) input.x += 1.0f;
        if (Input.GetKey(KeyCode.LeftArrow)) input.x -= 1.0f;
        if (Input.GetKey(KeyCode.UpArrow)) input.z += 1.0f;
        if (Input.GetKey(KeyCode.DownArrow)) input.z -= 1.0f;

        //make sure the input doesn't exceed 1 if we go diagonally
        if (input != Vector3.zero)
            input.Normalize();
    }

    public void ApplyImpulse(Vector3 direction, float force)
    {
        velocity += direction*force;
    }

    void Update()
    {
        if (LevelHandling.Instance.AllowInput)
        {
            if (UseGamepad)
                GetInput();
            else
            {
                GetKeyboardInput();
            }
            //Apply friction to velocity
            velocity -= velocity * friction * Time.deltaTime;

            //Apply acceleration to velocity
            velocity += input * acceleration * Time.deltaTime;

            //Apply velocity to position
            transform.position += velocity * Time.deltaTime;

            if (velocity.normalized.magnitude > 0.01f)
                transform.forward = velocity.normalized;
        }
        else
        {
            velocity = Vector3.zero;
        }

        var animator = GetComponentInChildren<Animator>();
        animator.SetFloat("Movement", velocity.magnitude / (acceleration / friction));
    }
}
