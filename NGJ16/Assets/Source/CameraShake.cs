﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour
{
    public bool debugMode = false;//Test-run/Call ShakeCamera() on start

    [SerializeField]
    private float defaultMagnitude;
    [SerializeField]
    private float defaultDuration;

    private float magnitude;
    private float duration;

    bool isRunning = false; //Is the coroutine running right now?


    public static CameraShake Instance;

    void Awake()
    {
        Instance = this;
    }


    void Start()
    {

        if (debugMode)
        {
            magnitude = defaultMagnitude;
            duration = defaultDuration;
            ShakeCamera();
        }
    }


    void ShakeCamera()
    {
        Shake();
    }

    public void Shake()
    {

        this.duration = defaultDuration;//Add to the current time.
        this.magnitude = defaultMagnitude;

        if (!isRunning) StartCoroutine(DoShake());//Only call the coroutine if it isn't currently running. Otherwise, just set the variables.
    }

    public void Shake(float amount, float duration)
    {
        
        this.duration = duration;//Add to the current time.
        this.magnitude = amount;

        if (!isRunning) StartCoroutine(DoShake());//Only call the coroutine if it isn't currently running. Otherwise, just set the variables.
    }


    IEnumerator DoShake()
    {

        float elapsed = 0.0f;

        Vector3 originalCamPos = Camera.main.transform.position;

        while (elapsed < duration)
        {

            elapsed += Time.deltaTime;

            float percentComplete = elapsed / duration;
            float damper = 1.0f - Mathf.Clamp(4.0f * percentComplete - 3.0f, 0.0f, 1.0f);

            // map value to [-1, 1]
            float x = Random.value * 2.0f - 1.0f;
            float y = Random.value * 2.0f - 1.0f;
            x *= magnitude * damper;
            y *= magnitude * damper;

            Camera.main.transform.position = new Vector3(originalCamPos.x + x, originalCamPos.y + y, originalCamPos.z);

            yield return null;
        }

        Camera.main.transform.position = originalCamPos;
    }

}