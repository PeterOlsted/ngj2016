﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class DanceFloor : MonoBehaviour
{

    public float SecondsBetweenSwitch;
    public List<Material> Materials = new List<Material>();

    private Renderer _rend;
    private int _curMatIndex = 0;

   // Use this for initialization
	void Start ()
	{
	    _rend = GetComponent<Renderer>();
	    StartCoroutine(SwitchDanceFloor());
	}
	
    private int nextSampleTime;
    private int lastSample = -1;
    private IEnumerator SwitchDanceFloor()
    {
        while (true)
        {
            AudioSource source = AudioEffects.Instance.Battle.PlayingInfo.Players.FirstOrDefault();
            if (source != null)
            {
                int beatSample = source.clip.samples/16;
                

                //Debug.Log(nextSampleTime + " " + beatSample + " "+ source.timeSamples + " " + source.clip.samples);
                //Debug.Log((source.timeSamples > beatSample).ToString() +" "+ (source.timeSamples > nextSampleTime).ToString());

                if (lastSample > source.timeSamples)
                {
                    nextSampleTime = beatSample;
                }
                lastSample = source.timeSamples;


                if (source.timeSamples > beatSample && source.timeSamples > nextSampleTime)
                {
                    nextSampleTime = nextSampleTime + beatSample;
                    _rend.material = Materials[_curMatIndex++ % Materials.Count];

                    if (nextSampleTime > source.clip.samples )
                    {
                        
                    }
                }
            }
            yield return null;
        }
    }
}
