﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using GamepadInput;
using InAudioSystem.ExtensionMethods;
using UnityEngine.UI;

public class LevelHandling : MonoBehaviour
{
    public float pointsTeamOne, matchPointsT1;
    public float pointsTeamTwo, matchPointsT2;
    public int maxPoints;
    public float pointsPerSecond = 5;
    public List<Player> teamOne;
    public List<Player> teamTwo;
    private int round = 0;
    public int maxRounds = 4;
    private bool squidTeamOne = false;
    private bool squidTeamTwo = false;
    public GameObject humanPrefab;
    public GameObject squidPrefab;
    public List<SpawnPoint> SpawnPoints;
    public static LevelHandling Instance;
    public ScoreUI ScoreUi;
    public bool AllowInput = false;
    public Text CountdownText;
    public Text WinnerText;
    public Material blueMaterial;
    public Material redMaterial;

    void Awake()
    {
        Instance = this;
        NextRound();
    }

    // Use this for initialization
    void Start()
    {
        ScoreUi.SetScore(0, 0, maxPoints);
    }

    
    void Update()
    {
        WhoIsSquid();
        if (AllowInput)
        {
            Points();
            WinConditionInkMeterFull();
        }

    }
    
    void WinConditionInkMeterFull()
    {
        if (pointsTeamOne >= maxPoints)
        {
            NextRound();
            return;
        }

        if (pointsTeamTwo >= maxPoints)
        {
            NextRound();
            return;
        }
    }

    public void NextRound()
    {
        StartCoroutine(DoNextRound());
    }

    IEnumerator DoNextRound()
    {
        AllowInput = false;
        round++;
        // Reset score.
        pointsTeamOne = 0;
        pointsTeamTwo = 0;
        ScoreUi.SetScore(pointsTeamOne, pointsTeamTwo, maxPoints);

        if (round > maxRounds)
        {
            StartCoroutine(ReadyScoreScreen());
        }
        else
        {
            ChangeSquidTeam();
            for (int i = 3; i >= 1; i--)
            {
                CountdownText.text = i.ToString();
                yield return new WaitForSeconds(0.5f);
            }
            CountdownText.text = "";
            AllowInput = true;
        }
    }

    IEnumerator ReadyScoreScreen()
    {
        WinnerText.gameObject.SetActive(true);
        yield return new WaitForSeconds(2.0f);
        if (GameHandler.Instance != null)
        {
            int highestPoints = (int)Mathf.Max(matchPointsT1, matchPointsT2);
            GameHandler.Instance.SetScores(Mathf.RoundToInt(matchPointsT1), Mathf.RoundToInt(matchPointsT2), highestPoints);
            GameHandler.Instance.GoToScoreScreen();
        }
    }

    void ChangeSquidTeam()
    {
        ClearGameObjects();

        if (round == 1)
        {
            var player2 = GameObject.Instantiate(squidPrefab);
            var player1 = GameObject.Instantiate(humanPrefab);
            
            teamOne.Add(player1.GetComponent<Player>());
            teamOne.Add(player2.GetComponent<Player>());

            var player3 = GameObject.Instantiate(humanPrefab);
            var player4 = GameObject.Instantiate(humanPrefab);
            teamTwo.Add(player3.GetComponent<Player>());
            teamTwo.Add(player4.GetComponent<Player>());
        }

        if (round == 2)
        {
            var player1 = GameObject.Instantiate(humanPrefab);
            var player2 = GameObject.Instantiate(humanPrefab);
            teamOne.Add(player1.GetComponent<Player>());
            teamOne.Add(player2.GetComponent<Player>());

            var player3 = GameObject.Instantiate(humanPrefab);
            var player4 = GameObject.Instantiate(squidPrefab);
            teamTwo.Add(player3.GetComponent<Player>());
            teamTwo.Add(player4.GetComponent<Player>());
        }

        if (round == 3)
        {
            var player1 = GameObject.Instantiate(squidPrefab);
            var player2 = GameObject.Instantiate(humanPrefab);
            teamOne.Add(player1.GetComponent<Player>());
            teamOne.Add(player2.GetComponent<Player>());

            var player3 = GameObject.Instantiate(humanPrefab);
            var player4 = GameObject.Instantiate(humanPrefab);
            teamTwo.Add(player3.GetComponent<Player>());
            teamTwo.Add(player4.GetComponent<Player>());
        }

        if (round == 4)
        {
            var player1 = GameObject.Instantiate(humanPrefab);
            var player2 = GameObject.Instantiate(humanPrefab);
            teamOne.Add(player1.GetComponent<Player>());
            teamOne.Add(player2.GetComponent<Player>());

            var player3 = GameObject.Instantiate(squidPrefab);
            var player4 = GameObject.Instantiate(humanPrefab);
            teamTwo.Add(player3.GetComponent<Player>());
            teamTwo.Add(player4.GetComponent<Player>());

            //InAudio.Music.Crossfade(AudioEffects.Instance.Battle, AudioEffects.Instance.BattleIntense, 0.8f);
        }
        
        
        SetPlayerInformation();
        
        

    }

    void ClearGameObjects()
    {
        foreach (Player player in teamOne)
        {
            GameObject.Destroy(player.gameObject);
        }

        foreach (Player player in teamTwo)
        {
            GameObject.Destroy(player.gameObject);
        }

        teamOne.Clear();
        teamTwo.Clear();
    }

    void WhoIsSquid()
    {
        foreach (Player player in teamOne)
        {
            if (player is Squid)
            {
                squidTeamOne = true;
                squidTeamTwo = false;
                return;
            }
        }

        squidTeamOne = false;
        squidTeamTwo = true;
    }

    void Points()
    {
        if (squidTeamOne)
        {
            if (pointsTeamOne < maxPoints)
            {
                pointsTeamOne += pointsPerSecond*Time.deltaTime;
                matchPointsT1 += pointsPerSecond*Time.deltaTime;
            }
        }

        if (squidTeamTwo)
        {
            if (pointsTeamTwo < maxPoints) {
                pointsTeamTwo += pointsPerSecond * Time.deltaTime;
                matchPointsT2 += pointsPerSecond * Time.deltaTime;
            }
        }

        // Update the UI.
        ScoreUi.SetScore(pointsTeamOne, pointsTeamTwo, maxPoints);
    }

    void SetPlayerInformation()
    {
        int index = 0;
        foreach (var player in teamOne)
        {
            player.transform.position = SpawnPoints.Find(s => s.Index == index).transform.position;
            index++;
            player.Index = (GamePad.Index) index;
            player.Team = Team.TEAM_ONE;
            player.GetComponentsInChildren<SkinnedMeshRenderer>().ForEach(m => m.materials = new Material[] { redMaterial, redMaterial });
        }

        foreach (var player in teamTwo)
        {
            player.transform.position = SpawnPoints.Find(s => s.Index == index).transform.position;
            index++;
            player.Index = (GamePad.Index)index;
            player.Team = Team.TEAM_TWO;
            player.GetComponentsInChildren<SkinnedMeshRenderer>().ForEach(m => m.materials = new Material[] { blueMaterial, blueMaterial });
        }
    }
}
