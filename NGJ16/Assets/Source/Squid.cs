﻿using UnityEngine;
using System.Collections;
using GamepadInput;

public class Squid : Player
{
    public float acceleration = 40;
    public float friction = 4;

    Vector3 velocity;
    Vector3 input;

    public void ApplyImpulse(Vector3 direction, float force)
    {
        velocity += direction * force;
    }

    void GetInput()
    {
        GamepadState state = GamePad.GetState(Index);
        input = Vector3.zero;

        input.x = state.LeftStickAxis.x;
        input.z = state.LeftStickAxis.y;

        if (input != Vector3.zero)
            input.Normalize();
    }

    void Update()
    {
        if (LevelHandling.Instance.AllowInput)
        {
            GetInput();
            //Apply friction to velocity
            velocity -= velocity*friction*Time.deltaTime;

            //Apply acceleration to velocity
            velocity += input*acceleration*Time.deltaTime;

            //Apply velocity to position
            transform.position += velocity*Time.deltaTime;
            if (velocity.normalized.magnitude > 0.01f)
                transform.forward = velocity.normalized;

        }
        else
        {
            velocity = Vector3.zero;
        }
        var animator = GetComponentInChildren<Animator>();
        animator.SetFloat("Movement", velocity.magnitude / (acceleration / friction));

    }
}
