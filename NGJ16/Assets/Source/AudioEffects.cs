﻿using UnityEngine;

public class AudioEffects : MonoBehaviour
{
    public InAudioNode Bounce;
    public InAudioNode Splash;
    public InAudioNode Point;

    public InAudioNode WinSwipe;
    public InAudioNode WinBling;

    public InMusicGroup Battle;
    //public InMusicGroup BattleIntense;
    public InMusicGroup MainMenu;
    public InMusicGroup ScoreScreen;


    void Awake()
    {
        Instance = this;
    }

    public static AudioEffects Instance;
}
